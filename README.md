# Arduino Leonardo on AtMega32u4 at 8MHz

This project is the custom configuration needed in Arduino in order to run an
Arduino Leonardo with an external 8MHz crystal instead of the 16MHz crystal it
has by default.

The project includes the needed bootloader for that too.



## Why would I use an Arduino Leonardo at 8MHz?

There are many reasons for it but one of the coolest is that you can make a
custom Arduino Leonardo compatible board at `3V3` instead of the default `5V`.
In order to do that the AtMega32u4 device can't run at 16MHz.
See [Atmega32u4 datasheet][1]

[1]: http://www.atmel.com/Images/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Datasheet.pdf#G18.1344509



## Installation

I didn't find a way to install this in an external folder *yet*.

This is the way I installed the project:

> Instructions are made for Unix-like systems. Windows should be similar.

``` bash
# Set arduino_installation to the directory where you have arduino installed
# In my case it's ~/arduino-1.8.5/

# Append the boards.txt to Arduino's boards.txt
cat avr/boards.txt >> ${arduino_installation}/hardware/arduino/avr/boards.txt
cp avr/bootloaders/caterina/Caterina-Leonardo-8MHz.hex ${arduino_installation}/hardware/arduino/avr/bootloaders/caterina/
```

## Compilation and description

The project uses the default `Caterina` bootloader which comes with the Arduino
installation. This means the C code is the same that the Arduino Leonardo has
but it's configured to run at 8MHz.

You can check the code in `avr/bootloaders/caterina` folder.

The only thing I changed is the `Makefile` where I had to change the `F_CPU`
and some descriptors. You can easily compare the Makefile with the default one
you have with you Arduino installation.

In order to compile, you need the [LUFA library][2]. Exactly the version `111009`,
but it might work with others. The `makefile` has a hardcoded path to the LUFA
library, just download it and put it there or change the path to the place you
have it.

You'll also need the `avrgcc` tool and some others, or you can use those who
came with your Arduino installation directly running this command before
compiling.

``` bash
export PATH=$PATH:${arduino_installation}/hardware/tools/avr/bin/
```

Run `make` and it'll create a Caterina.hex file (and some more). It should be
exactly the same you have precompiled.

> Note that the `boards.txt` file points to the bootloader name I've chosen so,
> if you compile it by yourself you need to rename it or change the
> `boards.txt` file to find the bootloader you compiled.

[2]: https://github.com/abcminiuser/lufa/


# LICENSE

All the changes I made are published as **public domain**. The `.c` and `.h`
files which are redistributed with this project have their own license (a
MIT-like one).
